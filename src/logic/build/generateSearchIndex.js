// Import libraries
import FlexSearch from "flexsearch";
import pMemoize from "p-memoize";

// Import data
import getDevices from "./getDevices.js";

async function generateSearchIndex(memoCode) {
  const devices = await getDevices("get-all-devices");

  const minimalData = devices
    .filter((d) => !d.unlisted)
    .map((device) => {
      return {
        id: device.codename + "@" + device.release,
        // Searchable
        codename: device.codename,
        name: device.name,

        // Sortable
        progress: device.progress,
        price: device.price?.avg,

        // Filterable
        deviceType: device.deviceType,
        portType: device.portType?.startsWith("Halium")
          ? "Halium"
          : device.portType,
        progressStage: device.progressStage.number,
        installerAvailable: !device.noInstall,
        waydroidAvailable:
          device.portStatus
            ?.find((el) => el.categoryName == "Misc")
            ?.features?.find((el) => el.id == "waydroid")?.value == "+",
        displayOutAvailable: device.isConvergenceEnabled,
        release: device.release
      };
    });

  var FSdoc = new FlexSearch.Document({
    profile: "default",
    tokenize: "forward",
    minlength: 2,
    document: {
      id: "id",
      index: ["codename", "name"],
      store: [
        "deviceType",
        "portType",
        "progressStage",
        "installerAvailable",
        "waydroidAvailable",
        "displayOutAvailable",
        "release"
      ]
    }
  });

  minimalData.forEach((device) => {
    FSdoc.add(device);
  });

  // Very bad code to make callback become sync
  // If anyone sees this, please change it
  var count = 0;
  let exportedIndex = [];
  FSdoc.export((k, d) => {
    exportedIndex.push({
      key: k,
      data: d ? d : null
    });
    console.log("  │  [Search index] Export #" + count + " " + k);
    count++;
  });

  function delay(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  while (count != 9) {
    await delay(50);
  }

  // End of very bad code block

  return exportedIndex;
}

export default pMemoize(generateSearchIndex);
