---
portType: "Halium 11.0"

portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "torchlight"
        value: "+"
      - id: "notificationLed"
        value: "+"
      - id: "vibration"
        value: "+"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "+"
      - id: "photo"
        value: "+"
      - id: "video"
        value: "+"
        bugTracker: "https://gitlab.com/ubports/development/core/qtubuntu-camera/-/issues/22"
      - id: "switchCamera"
        value: "+"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "+"
      - id: "dataConnection"
        value: "+"
      - id: "calls"
        value: "+-"
        bugTracker: "https://gitlab.com/ubports/porting/community-ports/android11/samsung-galaxy-s7/samsung-exynos8890/-/issues/2"
      - id: "dualSim"
        value: "+"
      - id: "mms"
        value: "+-"
        bugTracker: "https://gitlab.com/ubports/porting/community-ports/android11/samsung-galaxy-s7/samsung-exynos8890/-/issues/1"
      - id: "pinUnlock"
        value: "+"
      - id: "sms"
        value: "+"
      - id: "audioRoutings"
        value: "+"
        bugTracker: "https://github.com/ubports/pulseaudio-modules-droid-30/pull/1"
      - id: "voiceCall"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "+"
      - id: "noRebootTest"
        value: "?"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "+"
        bugTracker: "https://github.com/ubports/gst-plugins-bad-packaging/pull/4"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "?"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "+"
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "+"
      - id: "factoryReset"
        value: "+"
      - id: "rtcTime"
        value: "+"
      - id: "sdCard"
        value: "+"
      - id: "shutdown"
        value: "+"
      - id: "wirelessCharging"
        value: "+"
      - id: "wirelessExternalMonitor"
        value: "?"
      - id: "waydroid"
        value: "-"
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "+"
      - id: "flightMode"
        value: "+"
      - id: "fmRadio"
        value: "x"
      - id: "hotspot"
        value: "+"
        overrideGlobal: true
      - id: "nfc"
        value: "?"
      - id: "wifi"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "+"
        bugTracker: "https://gitlab.com/ubports/development/core/packaging/sensorfw/-/merge_requests/8"
      - id: "fingerprint"
        value: "+"
      - id: "gps"
        value: "+"
      - id: "proximity"
        value: "+"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "+"
      - id: "adb"
        value: "+"
      - id: "wiredExternalMonitor"
        value: "x"
---

## Credits

Contributors of https://github.com/8890q/ made this port possible with their LineageOS 18.1 porting effort

## Notes

- Kernel features for waydroid are enabled, but waydroid does not support Halium 11.0 currently, cannot test.
- Dual sim Galaxy S7 exynos can only use 3G/4G on one of the sim slot at a time. While it is possible on Android to pick a sim for 3G/4G, on this port, only sim1 can use 3G/4G, sim2 is 2G only.
- Fingerprint sensor drivers are patched to work with halium, but only tested on one of the possible sensor model. Create an issue if fingerprint sensor is not working for you.
