---
name: "OnePlus Nord 2 5G"
deviceType: "phone"

deviceInfo:
  - id: "arch"
    value: "arm64"
  - id: "cpu"
    value: "Octa-core (1x3.0 GHz Cortex-A78 & 3x2.6 GHz Cortex-A78 & 4x2.0 GHz Cortex-A55)"
  - id: "chipset"
    value: "MediaTek Dimensity 1200, MT6893"
  - id: "gpu"
    value: "ARM Mali-G77 MC9"
  - id: "rom"
    value: "128 GB, 256 GB"
  - id: "ram"
    value: "6 GB, 8 GB, 12 GB"
  - id: "android"
    value: "Android 11.0"
  - id: "battery"
    value: "4500 mAh"
  - id: "display"
    value: '6.43" Fluid AMOLED, 1080 x 2400 (410 PPI)'
  - id: "rearCamera"
    value: "50 MP Sony IMX766, 8 MP ultrawide, 2 MP monochrome"
  - id: "frontCamera"
    value: "32 MP Sony IMX615"
  - id: "dimensions"
    value: "158.9 x 73.2 x 8.3 mm (6.26 x 2.88 x 0.33 in)"
  - id: "weight"
    value: "189 g (6.67 oz)"
  - id: "releaseDate"
    value: "July 2021"

externalLinks:
  - name: "Telegram group"
    link: "https://t.me/ut_oneplusnord2"
  - name: "Device Repositories"
    link: "https://gitlab.com/ubports/porting/community-ports/android11/oneplus-nord-2"
  - name: "Report a bug"
    link: "https://gitlab.com/ubports/porting/community-ports/android11/oneplus-nord-2/oneplus-denniz/-/issues"
contributors:
  - name: TheKit
    role: Maintainer
    renewals:
      - "2022-10-21"
    forum: https://forums.ubports.com/user/thekit
    photo: ""
---
