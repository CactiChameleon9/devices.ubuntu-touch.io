---
name: "Motorola Moto Z2 Force"
deviceType: "phone"
description: "The Moto Z2 Force is a very performant device, still light and thin and they claim, that it has an unbreakable display."

deviceInfo:
  - id: "cpu"
    value: "Octa-core 64-bit"
  - id: "chipset"
    value: "Qualcomm Snapdragon 835"
  - id: "gpu"
    value: "Qualcomm Adreno 540"
  - id: "rom"
    value: "64GB"
  - id: "ram"
    value: "6GB"
  - id: "android"
    value: "Android 7.1.1"
  - id: "battery"
    value: "2730 mAh"
  - id: "display"
    value: "1.440 x 2.560 pixels, 5.5 in, 534 PPI, AMOLED"
  - id: "rearCamera"
    value: "12MP"
  - id: "frontCamera"
    value: "8MP"
  - id: "dimensions"
    value: "155,8 x 76 x 5,99 mm"
  - id: "weight"
    value: "143,0 g"
  - id: "arch"
    value: "arm64"
  - id: "releaseDate"
    value: "August 2017"

contributors:
  - name: "Luksus"
    photo: ""
    forum: "https://forums.ubports.com/user/luksus"

externalLinks:
  - name: "Repository"
    link: "https://gitlab.com/ubports/porting/community-ports/android9/motorola-sdm660/motorola-nash"
---
