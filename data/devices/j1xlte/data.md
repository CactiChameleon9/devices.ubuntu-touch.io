---
name: "Samsung Galaxy J1 (2016)"
deviceType: "phone"

#[description: <marketing-style description that helps a user decide if this device is right for them>]
#[buyLink: <link to manufacturer's store, if it is available>]
disableBuyLink: true
#[price: <average price of the device in dollars>]
#[subforum: <the subforum id on forums.ubports.com, should be added in the format number/my-device>]
#[tag: <promoted|unmaintained>]

deviceInfo:
  - id: "cpu"
    value: "Quad-core 1.3 GHz Cortex-A7"
  - id: "chipset"
    value: "Samsung Exynos 3 Quad 3475"
  - id: "gpu"
    value: "Mali-T720"
  - id: "ram"
    value: "1 GB"
  - id: "rom"
    value: "8 GB"
  - id: "android"
    value: "Android 5, officially upgradeable to 6"
  - id: "battery"
    value: "2050 mAh, replaceable"
  - id: "display"
    value: '4.5", 800x480'
  - id: "rearCamera"
    value: "5 MP"
  - id: "frontCamera"
    value: "2 MP"
  - id: "arch"
    value: "ARM"
  - id: "dimensions"
    value: '5.22" x 2.73" x 0.35"'
  - id: "weight"
    value: "4.62 oz"
  - id: "releaseDate"
    value: "Febuary 1st, 2016"
contributors:
  - name: "j1xltegtelwifiue"
    forum: "https://forums.ubports.com/user/j1xltegtelwifiue"
    photo: ""
communityHelp: # Links for the user to get help during installation and usage
  - name: "UBports Welcome & Install"
    link: "https://t.me/WelcomePlus"
externalLinks: # Development resources
  - name: "Instructions, issues, and contributions"
    link: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte"
---
